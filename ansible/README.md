# Ansible Command #

* Test instalasi dan login ke remote server

    ```
    ansible bukutamu -m ping
    ```

* Install Nginx

    ```
    ansible-playbook nginx.yml
    ```

## Referensi ##

* [Dokumentasi Resmi Ansible](https://docs.ansible.com/ansible/latest/index.html)
* [Ansible Tutorial](https://serversforhackers.com/c/an-ansible-tutorial)
* [Instalasi Nginx](https://code-maven.com/install-and-configure-nginx-using-ansible)
* [Tutorial Playbook](https://www.digitalocean.com/community/tutorials/configuration-management-101-writing-ansible-playbooks)
* [Struktur Folder Playbook](https://dev.to/tmidi/ansible-directory-layout-5edj)
* [Best Practices](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html)
