package com.muhardin.endy.training.automatedtest.aplikasibukutamu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiBukutamuApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiBukutamuApplication.class, args);
	}

}
