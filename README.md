# Training DevOps 201902 #

Setting Koneksi Database Local

* Username : `bukutamu`
* Password : `bukutamu123`
* Host Server : `localhost`
* Nama Database : `bukutamudb`

Cara membuat username database di localhost

```
grant all on bukutamudb.* to bukutamu@localhost identified by 'bukutamu123';
```

Cara membuat database di localhost

```
create database bukutamudb;
```

Cara menjalankan aplikasi

```
mvn spring-boot:run
```

## Deployment Pivotal ##

* Username : trainingdevops201902@yopmail.com
* Password : JA9zUWt1dg-

1. Create database MySQL dari layanan ClearDB menggunakan paket Spark yang gratis

    ```
    cf create-service cleardb spark aplikasi-bukutamu-db
    ``` 

2. Bind database ke aplikasi

    ```
    cf bs aplikasi-bukutamu aplikasi-bukutamu-db
    ```

3. Build aplikasi

    ```
    mvn clean package -DskipTests
    ```

4. Deploy Aplikasi

    ```
    cf push
    ```

5. Pantau log aplikasi untuk mengantisipasi bila ada error

    ```
    cf logs aplikasi-bukutamu
    ```

6. Bila lancar, browse ke halaman aplikasi [aplikasi-bukutamu-fantastic-okapi.cfapps.io/](aplikasi-bukutamu-fantastic-okapi.cfapps.io/)

## Docker Development Workflow ##

Membuat docker image

1. Build jar dulu

    ```
    mvn clean package -DskipTests
    ```

2. Build docker image

    ```
    docker build -t endymuhardin/aplikasi-bukutamu .
    ```

3. Run docker image

    ```
    docker run endymuhardin/aplikasi-bukutamu:latest
    ```

4. Login ke docker registry
 
    ```
    docker login
    Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
    Username: endymuhardin
    Password: 
    WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
    Configure a credential helper to remove this warning. See
    https://docs.docker.com/engine/reference/commandline/login/#credentials-store

    Login Succeeded
    ```

5. Upload / push ke registry

    ```
    docker push endymuhardin/aplikasi-bukutamu
    The push refers to repository [docker.io/endymuhardin/aplikasi-bukutamu]
    116683870bd8: Pushed 
    5350597c2695: Pushed 
    ceaf9e1ebef5: Mounted from library/openjdk 
    9b9b7f3d56a0: Mounted from library/openjdk 
    f1b5933fe4b5: Mounted from library/openjdk 
    latest: digest: sha256:03baf94a4e7aed08bee195e064151fd9e8b5286edf87734a9671929f02c6e3b8 size: 1371
    ```